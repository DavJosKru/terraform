output "Master" {
  value = element(virtualbox_vm.BGA-Master.*.network_adapter.1.ipv4_address, 1)
}

output "Worker_1" {
  value = element(virtualbox_vm.BGA-Worker.*.network_adapter.1.ipv4_address, 2)
}

output "Worker_2" {
  value = element(virtualbox_vm.BGA-Worker.*.network_adapter.1.ipv4_address, 3)
}
