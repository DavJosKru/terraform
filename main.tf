resource "virtualbox_vm" "BGA-Master" {
  count     = 1
  name      = format("BGA-Master-%02d", count.index + 1)
  image     = "../packer/builds/centos7.box"
  cpus      = 2
  memory    = "2048 mib"

  network_adapter {
    type           = "nat"
  }

  network_adapter {
    type           = "hostonly"
    host_interface = "vboxnet0"
  }
}

resource "virtualbox_vm" "BGA-Worker" {
  count     = 2
  name      = format("BGA-Worker-%02d", count.index + 1)
  image     = "../packer/builds/centos7.box"
  cpus      = 2
  memory    = "2048 mib"

  network_adapter {
    type           = "nat"
  }

  network_adapter {
    type           = "hostonly"
    host_interface = "vboxnet0"
  }
}
